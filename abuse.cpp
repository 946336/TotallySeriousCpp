#include "name_abuse.h"

int main()
{
    Contrived<int> map;
    map.push_back(1);
    map.push_back(2);
    map.push_back(3);

    // Payoff: elementwise doubling of the container contents
    map = map(map, map);

    std::cout << map << std::endl;

    return 0;
}
