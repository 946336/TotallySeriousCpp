
CXXFLAGS = -Wall -Wextra -pedantic -std=c++0x

abuse: name_abuse.h abuse.cpp
	$(CXX) $(CXXFLAGS) $^ -o $@

clean:
	rm -f abuse

