#ifndef NAME_ABUSE_H
#define NAME_ABUSE_H

// Welcome to a facilitator for the abuse of names
// This apparently works as is all the way down to c++0x
// Please remember to cleanse your eyes after reading this file
//
// This was an exercise in C++ name resolution order. The whole point is to be
// able to legally write: map = map(map, map); and have something useful happen

#include <vector>
#include <iostream>

// T must have operator+ defined on itself
template <typename T>
class Functor {
public:
    Functor() {}
    ~Functor() {}

    // This is the function applied in the payoff
    T operator()(T const &i) {return i + i;}
private:
    // If you have need for a closure that operator() can use, that goes here
};

template <typename T>
class Contrived : public std::vector<T>, public Functor<T> {
public:
    Contrived() {}
    ~Contrived() {}

    // RIP
    Contrived<T> operator()(Contrived<T> c, Functor<T> f) const
        { return map(c, f); }

    template<typename R>
    friend std::ostream &operator<<(std::ostream &os, Contrived<R> const &c);
private:
    // If you need it, I guess
};

template<typename T>
static Contrived<T> map(Contrived<T> const &container, Functor<T> &func)
{
    Contrived<T> ret = Contrived<T>();
    ret.reserve(container.size());
    for (auto &i : container) ret.push_back(func(i));
    return ret;
}

template<typename T>
std::ostream &operator<<(std::ostream &os, Contrived<T> const &c)
{
    for (auto const &i : c) os << i;
    return os;
}

#endif
