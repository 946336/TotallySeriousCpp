# abuse

`abuse` serves as a discussion piece and should never be taken seriously in any
capacity. Realize that the entire point was to arrive at the punchline and not
to do anything useful.

The punchline will identify itself as you read.

-------------------
## To build

You'll need a compiler that supports at least c++0x (compiled but not otherwise
tested with g++ 4.8, 4.9, and 5.4.1)

`make abuse`
